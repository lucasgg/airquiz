//
//  main.m
//  AirQuiz
//
//  Created by Lucas Guimaraes Gonsalves on 18/03/15.
//  Copyright (c) 2015 LGG. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
