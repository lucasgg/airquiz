//
//  SaveAppData.h
//  AirQuiz
//
//  Created by Lucas Guimaraes Gonsalves on 20/03/15.
//  Copyright (c) 2015 LGG. All rights reserved.
//

#import <Foundation/Foundation.h>

@class UserScore;

@interface SaveAppData : NSObject

+ (void)loadData;
+ (void)saveData;
+ (void)restartData;

@end
