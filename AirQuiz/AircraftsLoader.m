//
//  AircraftsLoader.m
//  AirQuiz
//
//  Created by Lucas Guimaraes Gonsalves on 20/03/15.
//  Copyright (c) 2015 LGG. All rights reserved.
//

#import "AircraftsLoader.h"
#import "Aircraft.h"

@implementation AircraftsLoader {
	NSMutableArray *enterprizes;
}

+ (id)sharedSingleton {
	static AircraftsLoader *sharedSingleton = nil;
	
	static dispatch_once_t once;
	
	dispatch_once(&once, ^{
		sharedSingleton = [[AircraftsLoader alloc] init];
	});
	
	return sharedSingleton;
}

- (instancetype)init {
	self = [super init];
	if (self) {
		enterprizes = [[NSMutableArray alloc] init];
		[self populate];
	}
	return self;
}

- (NSInteger)enterprizeCount {
	return enterprizes.count;
}

- (NSInteger)aircraftCount:(NSInteger)enterprize {
	if (enterprize < enterprizes.count)
		return ((NSArray *) [enterprizes objectAtIndex:enterprize]).count;
	else
		return 0;
}

- (Aircraft *)getAircraftFromEnterprize:(NSInteger)enterprize ofPosition:(NSInteger)pos {
	if (enterprize < [self enterprizeCount] && pos < [self aircraftCount:enterprize])
		return (Aircraft *) [((NSArray *) [enterprizes objectAtIndex:enterprize]) objectAtIndex:pos];
	else
		return nil;
}

- (void)populate {
	// Lê os arquivos e popula os dados do array.
	NSBundle *bundle = [NSBundle mainBundle];
	NSError *erro;
	
	NSArray *aircraft_files = [NSArray arrayWithObjects:@"cessna", @"embraer", nil];
	
	for (NSInteger i = 0; i < aircraft_files.count; i++) {
		NSString *path = [bundle pathForResource:[aircraft_files objectAtIndex:i] ofType:@"txt"];
		NSString *arq = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:&erro];
		
		// Get the lines of the archive.
		NSArray *enterprize_lines = [arq componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]];
		
		NSMutableArray *enterprize_aircrafts = [[NSMutableArray alloc] init];
		
		for (NSInteger j = 0; j < enterprize_lines.count; j++) {
			// Verify if line is nil.
			if ([[enterprize_lines objectAtIndex:j] isEqualToString:@""]) {
				continue;
			}
			
			// NSLog(@"%d, %d - %@", i, j, [enterprize_lines objectAtIndex:j]);
			
			NSArray *dados = [[enterprize_lines objectAtIndex:j] componentsSeparatedByString:@"|"];
			
			NSString *aircraft_model = (NSString *) [dados objectAtIndex:0];
			
			NSString *aircraft_type_text = ((NSString *) [dados objectAtIndex:1]);
			NSInteger aircraft_type;
			if ([aircraft_type_text isEqualToString:@"JATO_EXECUTIVO"])
				aircraft_type = JATO_EXECUTIVO;
			else if ([aircraft_type_text isEqualToString:@"TURBO_EXECUTIVO"])
				aircraft_type = TURBO_EXECUTIVO;
			else if ([aircraft_type_text isEqualToString:@"MULTI_USO"])
				aircraft_type = MULTI_USO;
			else if ([aircraft_type_text isEqualToString:@"AGRICOLA"])
				aircraft_type = AGRICOLA;
			else if ([aircraft_type_text isEqualToString:@"PASSAGEIRO"])
				aircraft_type = PASSAGEIRO;
			else if ([aircraft_type_text isEqualToString:@"MILITAR"])
				aircraft_type = MILITAR;
			else if ([aircraft_type_text isEqualToString:@"PESSOAL_TREINAMENTO"])
				aircraft_type = PESSOAL_TREINAMENTO;
			else
				break;
			
			NSString *aircraft_motor_text = ((NSString *) [dados objectAtIndex:2]);
			NSInteger aircraft_motor;
			if ([aircraft_motor_text isEqualToString:@"MONOMOTOR"])
				aircraft_motor = MONOMOTOR;
			else if ([aircraft_motor_text isEqualToString:@"BIMOTOR"])
				aircraft_motor = BIMOTOR;
			else if ([aircraft_motor_text isEqualToString:@"TURBO_HELICE"])
				aircraft_motor = TURBO_HELICE;
			else
				break;
			
			NSInteger aircraft_decade = [((NSString *) [dados objectAtIndex:3]) integerValue];
			NSString *aircraft_width = (NSString *) [dados objectAtIndex:4];
			NSInteger aircraft_velocity = [((NSString *) [dados objectAtIndex:5]) integerValue];
			NSString *aircraft_image = (NSString *) [dados objectAtIndex:6];
			
			[enterprize_aircrafts addObject:[Aircraft aircraftWithModel:aircraft_model andType:aircraft_type andMotor:aircraft_motor andDecade:aircraft_decade andWidth:aircraft_width andVelocity:aircraft_velocity andImage:aircraft_image]];
		}
		
		[enterprizes addObject:[NSArray arrayWithArray:enterprize_aircrafts]];
	}
	
}

@end
