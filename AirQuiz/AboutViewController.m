//
//  AboutViewController.m
//  AirQuiz
//
//  Created by Lucas Guimaraes Gonsalves on 18/03/15.
//  Copyright (c) 2015 LGG. All rights reserved.
//

#import "AboutViewController.h"
#import "SaveAppData.h"

@interface AboutViewController ()

@property (weak, nonatomic) IBOutlet UITextView *creditsBox;

@end

@implementation AboutViewController

- (void)viewDidLoad {
	[super viewDidLoad];
	// Do any additional setup after loading the view.
	
	// Insert contents of the credits.txt into the textbox.
	NSBundle *bundle = [NSBundle mainBundle];
	NSError *erro;
	NSString *path = [bundle pathForResource:@"credits" ofType:@"txt"];
	
	[_creditsBox insertText:[NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:&erro]];
	
	// Round the credits textfield border.
	[[_creditsBox layer] setCornerRadius:4.0];
}

- (IBAction)RestartGameButton:(id)sender {
	// Show an alert about the restarting of the game.
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alerta" message:@"Deseja mesmo recomeçar a pontuação do zero?" delegate:self cancelButtonTitle:@"NÃO" otherButtonTitles:@"SIM", nil];
	[alert show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
	// 0 for NO.
	// 1 for YES;
	if (buttonIndex == 1) {
		[SaveAppData restartData];
	}
}

- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

@end
