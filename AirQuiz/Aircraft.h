//
//  Aircraft.h
//  AirQuiz
//
//  Created by Lucas Guimaraes Gonsalves on 20/03/15.
//  Copyright (c) 2015 LGG. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, AIRCRAFT_TYPE) {
	JATO_EXECUTIVO,
	TURBO_EXECUTIVO,
	MULTI_USO,
	AGRICOLA,
	PASSAGEIRO,
	MILITAR,
	PESSOAL_TREINAMENTO
};

typedef NS_ENUM(NSInteger, AIRCRAFT_MOTOR) {
	MONOMOTOR,
	BIMOTOR,
	TURBO_HELICE
};

@interface Aircraft : NSObject

@property (readonly) NSString *model;
@property (readonly) NSInteger type;
@property (readonly) NSInteger motor;
@property (readonly) NSInteger decade;
@property (readonly) NSString *width;
@property (readonly) NSInteger velocity;
@property (readonly) NSString *image;

+ (Aircraft *)aircraftWithModel:(NSString *)model andType:(NSInteger)type andMotor:(NSInteger)motor andDecade:(NSInteger)decade andWidth:(NSString *)width andVelocity:(NSInteger)velocity andImage:(NSString *)image;

- (instancetype)initWithModel:(NSString *)model andType:(NSInteger)type andMotor:(NSInteger)motor andDecade:(NSInteger)decade andWidth:(NSString *)width andVelocity:(NSInteger)velocity andImage:(NSString *)image;

@end
