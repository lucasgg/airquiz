//
//  SaveAppData.m
//  AirQuiz
//
//  Created by Lucas Guimaraes Gonsalves on 20/03/15.
//  Copyright (c) 2015 LGG. All rights reserved.
//

#import "SaveAppData.h"
#import "UserScore.h"

NSString * const USER_SCORE_KEY = @"userScore";
NSString * const USER_LEVELS_KEY = @"userLevels";

@implementation SaveAppData

+ (void)loadData {
	NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
	
	// Made the default read to verify if this is the first run of the app.
	NSObject *defaultsRead = [userDefaults objectForKey:USER_SCORE_KEY];
	
	if (defaultsRead == nil) {
		NSLog(@"First run."); // ashjdgajshgdjhasjdh
		
		// Create NSDictionary for app defaults.
		NSDictionary *appDefaults = [NSDictionary dictionaryWithObjectsAndKeys:
					     [NSNumber numberWithUnsignedInteger:0], USER_SCORE_KEY,
					     [NSArray arrayWithArray:[UserScore defaultArrayOfLevels]], USER_LEVELS_KEY,
					     nil];
		
		// Register and synchronize defaults.
		[userDefaults registerDefaults:appDefaults];
		[userDefaults synchronize];
	}
	
	NSNumber *userScore = (NSNumber *) [userDefaults objectForKey:USER_SCORE_KEY];
	NSArray *userLevels = (NSArray *) [userDefaults objectForKey:USER_LEVELS_KEY];
	
	NSLog(@"%d", [userScore integerValue]);
	
	[UserScore sharedSingletonFromScore:[userScore integerValue] andLevels:userLevels];
}

+ (void)saveData {
	NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
	UserScore *userScoreSingleton = [UserScore sharedSingleton];
	
	NSNumber *userScore = [NSNumber numberWithInteger:userScoreSingleton.score];
	NSArray *userLevels = [NSArray arrayWithArray:userScoreSingleton.levels];
	
	// Set the data to the defaults.
	[userDefaults setObject:userScore forKey:USER_SCORE_KEY];
	[userDefaults setObject:userLevels forKey:USER_LEVELS_KEY];
	
	// Synchronize defaults.
	[userDefaults synchronize];
}

+ (void)restartData {
	[[UserScore sharedSingleton] resetValuesToDefault];
}

@end
