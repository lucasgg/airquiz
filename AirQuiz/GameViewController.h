//
//  GameViewController.h
//  AirQuiz
//
//  Created by Lucas Guimaraes Gonsalves on 19/03/15.
//  Copyright (c) 2015 LGG. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GameViewController : UIViewController <UIPickerViewDataSource, UIPickerViewDelegate, UIAlertViewDelegate>

@end
