//
//  AirQuizQuestion.m
//  AirQuiz
//
//  Created by Lucas Guimaraes Gonsalves on 20/03/15.
//  Copyright (c) 2015 LGG. All rights reserved.
//

#import "AirQuizQuestion.h"

@implementation AirQuizQuestion

+ (AirQuizQuestion *)quizWithText:(NSString *)text andIsTextOnly:(BOOL)isTextOnly andType:(NSInteger)type {
	AirQuizQuestion *question = [[AirQuizQuestion alloc] initWithText:text andIsTextOnly:isTextOnly andType:type];
	
	return question;
}

- (instancetype)initWithText:(NSString *)text andIsTextOnly:(BOOL)isTextOnly andType:(NSInteger)type {
	self = [super init];
	if (self) {
		_question_text = text;
		_isTextOnly = isTextOnly;
		_type = type;
	}
	return self;
}

@end
