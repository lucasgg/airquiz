//
//  AirQuizGame.m
//  AirQuiz
//
//  Created by Lucas Guimaraes Gonsalves on 21/03/15.
//  Copyright (c) 2015 LGG. All rights reserved.
//

#import "AirQuizGame.h"
#import "AircraftsLoader.h"
#import "AirQuizQuestion.h"
#import "Aircraft.h"

@implementation AirQuizGame {
	AircraftsLoader *aircraftLoader;
	NSArray *airQuizQuestions;
}

+ (AirQuizGame *)sharedSingleton {
	static AirQuizGame *sharedSingleton = nil;
	
	static dispatch_once_t once;
	
	dispatch_once(&once, ^{
		sharedSingleton = [[AirQuizGame alloc] init];
	});
	
	return sharedSingleton;
}

- (instancetype)init
{
	self = [super init];
	if (self) {
		aircraftLoader = [AircraftsLoader sharedSingleton];
		_enterprize = 0;
		_level = 0;
		
		[self populate];
	}
	return self;
}

- (void)createGameOfEnterprize:(NSInteger)enterprize andLevel:(NSInteger)level {
	if (enterprize < [aircraftLoader enterprizeCount])
		_enterprize = enterprize;
	else
		_enterprize = 0;
	
	_level = level;
}

- (void)newQuestionOfType:(NSInteger)question_type {
	// Use this with caution, first create game with createGameOfEnterprize.
	NSInteger choosed_aircraft = arc4random() % [aircraftLoader aircraftCount:_enterprize];
	
	_question = [self getQuestionAboutEnterprize:_enterprize andAircraft:choosed_aircraft ofQuestionType:question_type];
	_question_image = [self getImageOfAircraftOfEnterprize:_enterprize andAircraft:choosed_aircraft];
	_aircraftAnswer = [aircraftLoader getAircraftFromEnterprize:_enterprize ofPosition:choosed_aircraft];
}

- (NSArray *)getFourDifferentAircrafts {
	// Caution, use this after newQuestionOfType.
	NSInteger choosed_aircraft;
	NSMutableArray *aircrafts = [[NSMutableArray alloc] init];
	
	while (aircrafts.count < 4) {
		choosed_aircraft = arc4random() % [aircraftLoader aircraftCount:_enterprize];
		
		Aircraft *aircraft = [aircraftLoader getAircraftFromEnterprize:_enterprize ofPosition:choosed_aircraft];
		
		if (aircraft != _aircraftAnswer) {
			BOOL add_object = YES;
			
			for (NSInteger j = 0; j < aircrafts.count; j++) {
				if (((Aircraft *) [aircrafts objectAtIndex:j]) == aircraft) {
					add_object = NO;
					break;
				}
			}
			
			if (add_object) {
				[aircrafts addObject:aircraft];
			}
		}
	}
	
	return [NSArray arrayWithArray:aircrafts];
}

- (void)populate {
	// Read the question files and populate the question array.
	NSBundle *bundle = [NSBundle mainBundle];
	NSError *erro;
	NSString *path = [bundle pathForResource:@"questions" ofType:@"txt"];
	NSString *arq = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:&erro];
	
	// Get the lines of the archive.
	NSArray *question_lines = [arq componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]];
	
	NSMutableArray *questions = [[NSMutableArray alloc] init];
	
	for (NSInteger i = 0; i < question_lines.count; i++) {
		// Verify if line is nil.
		if ([[question_lines objectAtIndex:i] isEqualToString:@""]) {
			continue;
		}
		
		// NSLog(@"%@", [question_lines objectAtIndex:i]);
		
		NSArray *dados = [[question_lines objectAtIndex:i] componentsSeparatedByString:@"|"];
		
		NSString *question_isTextOnly_text = ((NSString *) [dados objectAtIndex:0]);
		BOOL question_isTextOnly;
		if ([question_isTextOnly_text isEqualToString:@"AIRCRAFT"])
			question_isTextOnly = NO;
		else if ([question_isTextOnly_text isEqualToString:@"ONLYTEXT"])
			question_isTextOnly = YES;
		else
			break;
		
		NSString *question_text = (NSString *) [dados objectAtIndex:1];
		
		NSString *question_type_text = ((NSString *) [dados objectAtIndex:2]);
		NSInteger question_type;
		if ([question_type_text isEqualToString:@"MODEL"])
			question_type = MODEL_QUESTION;
		else if ([question_type_text isEqualToString:@"MOTOR"])
			question_type = MOTOR_QUESTION;
		else if ([question_type_text isEqualToString:@"YEAR"])
			question_type = YEAR_QUESTION;
		else if ([question_type_text isEqualToString:@"TYPE"])
			question_type = TYPE_QUESTION;
		else if ([question_type_text isEqualToString:@"VELOCITY"])
			question_type = VELOCITY_QUESTION;
		else
			break;
		
		[questions addObject:[AirQuizQuestion quizWithText:question_text andIsTextOnly:question_isTextOnly andType:question_type]];
	}
	
	airQuizQuestions = [NSArray arrayWithArray:questions];
}

- (NSString *)getQuestionAboutEnterprize:(NSInteger)enterprize andAircraft:(NSInteger)aircraftIndex ofQuestionType:(NSInteger)questionType {
	AirQuizQuestion *question;
	Aircraft *aircraft = [[AircraftsLoader sharedSingleton] getAircraftFromEnterprize:enterprize ofPosition:aircraftIndex];
	
	switch (questionType) {
		case MODEL_QUESTION:
			question = [airQuizQuestions objectAtIndex:0];
			
			return [NSString stringWithString:question.question_text];
			
			break;
			
		case MOTOR_QUESTION:
			question = [airQuizQuestions objectAtIndex:1];
			
			return [NSString stringWithFormat:question.question_text, aircraft.model];
			
			break;
			
		case YEAR_QUESTION:
			question = [airQuizQuestions objectAtIndex:2];
			
			return [NSString stringWithFormat:question.question_text, aircraft.model];
			
			break;
			
		case TYPE_QUESTION:
			question = [airQuizQuestions objectAtIndex:3];
			
			return [NSString stringWithFormat:question.question_text, aircraft.model];
			
			break;
			
		case VELOCITY_QUESTION:
			question = [airQuizQuestions objectAtIndex:4];
			
			return [NSString stringWithFormat:question.question_text, aircraft.model];
			
			break;
			
		default:
			NSLog(@"Erro, não existe esse tipo de pergunta.");
			
			return nil;
			
			break;
	}
}

- (NSString *)getImageOfAircraftOfEnterprize:(NSInteger)enterprize andAircraft:(NSInteger)aircraftIndex {
	return [[AircraftsLoader sharedSingleton] getAircraftFromEnterprize:enterprize ofPosition:aircraftIndex].image;
}

@end
