//
//  ViewController.m
//  AirQuiz
//
//  Created by Lucas Guimaraes Gonsalves on 18/03/15.
//  Copyright (c) 2015 LGG. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@property (weak, nonatomic) IBOutlet UIButton *airquizButton;

@end

@implementation ViewController {
	NSTimer *timer;
}

- (void)viewDidLoad {
	[super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
	
	// Timer for jump this logo screen.
	timer = [NSTimer scheduledTimerWithTimeInterval:3.0 target:self selector:@selector(toMainScreen) userInfo:nil repeats:NO];
	
	// Decorations...
	// Set initial button border and corner radius.
	[[_airquizButton layer] setBorderColor:[[UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:1.0] CGColor]];
	[[_airquizButton layer] setBorderWidth:2.0];
	[[_airquizButton layer] setCornerRadius:30.0];
}

- (IBAction)toMainScreenButtonAction:(id)sender {
	// Button action to jump the logo screen.
	[self toMainScreen];
}

- (void)toMainScreen {
	// Invalidade timer in any condition.
	[timer invalidate];
	timer = nil;
	
	// Go to main screen.
	[self performSegueWithIdentifier:@"toMainScreen" sender:self];
}

- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

@end
