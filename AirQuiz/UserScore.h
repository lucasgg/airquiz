//
//  UserScore.h
//  AirQuiz
//
//  Created by Lucas Guimaraes Gonsalves on 20/03/15.
//  Copyright (c) 2015 LGG. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, Levels) {
	LEVEL_CESSNA,
	LEVEL_EMBRAER,
	LEVEL_BOMBARDIER,
	LEVEL_BOEING,
	LEVEL_AIRBUS,
	LEVEL_ALL_AIRCRAFTS
};

typedef NS_ENUM(NSInteger, PlayerLevel) {
	NOOB,
	APRENTICE,
	BASIC,
	INTERMEDIATE,
	EXPERT,
	MASTER
};

@interface UserScore : NSObject

@property (readonly) NSUInteger score;
@property (readonly) NSMutableArray *levels;

+ (UserScore *)sharedSingletonFromScore:(NSInteger)score andLevels:(NSArray *)levels;
+ (UserScore *)sharedSingleton;
+ (NSArray *)defaultArrayOfLevels;

- (void)incrementScore;
- (void)incrementLevel:(Levels)aircraft;
- (NSInteger)getLevel:(Levels)aircraft;
- (void)resetValuesToDefault;
- (NSString *)descriptionForLevel:(NSInteger)level;
- (NSString *)imageForAircraft:(NSInteger)aircraft andLevel:(NSInteger)level;

@end
