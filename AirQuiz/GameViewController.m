//
//  GameViewController.m
//  AirQuiz
//
//  Created by Lucas Guimaraes Gonsalves on 19/03/15.
//  Copyright (c) 2015 LGG. All rights reserved.
//

#import "GameViewController.h"
#import "UserScore.h"
#import "AirQuizGame.h"
#import "AirQuizQuestion.h"
#import "Aircraft.h"

@interface GameViewController ()

@property (weak, nonatomic) IBOutlet UILabel *lblPontuacao;
@property (weak, nonatomic) IBOutlet UILabel *lblQuestion;
@property (weak, nonatomic) IBOutlet UIImageView *imageAircraft;
@property (weak, nonatomic) IBOutlet UIPickerView *picker;
@property (weak, nonatomic) IBOutlet UILabel *wins;


@end

@implementation GameViewController {
	UserScore *userScore;
	AirQuizGame *quizGame;
	
	NSInteger correctAnswer;
	NSInteger winCount;
	BOOL firstPlay;
	BOOL happyPlay;
	NSInteger numberOfItensInPicker;
	NSInteger selectedRow;
	
	NSMutableArray *pickerTexts;
}

- (void)viewDidLoad {
	[super viewDidLoad];
	// Do any additional setup after loading the view.
	
	userScore = [UserScore sharedSingleton];
	quizGame = [AirQuizGame sharedSingleton];
	
	// Set own variables.
	correctAnswer = 0;
	winCount = 0;
	numberOfItensInPicker = 0;
	selectedRow = 0;
	
	firstPlay = YES;
	happyPlay = NO;
}

- (void)viewWillAppear:(BOOL)animated {
	[self updateView];
}

- (void)updateView {
	// This is a custom function to update all labels, buttons and etc...
	
	// Set the score in label.
	_lblPontuacao.text = [NSString stringWithFormat:@"%d", userScore.score];
	
	// Set wincount.
	if (firstPlay) {
		[_wins setText:[NSString stringWithFormat:@"%d/6", winCount]];
		firstPlay = NO;
	} else {
		if (happyPlay == YES) {
			[_wins setText:[NSString stringWithFormat:@"😀 %d/6", winCount]];
		} else {
			[_wins setText:[NSString stringWithFormat:@"😕 %d/6", winCount]];
		}
	}
	
	
	// Reload game information.
	{
		// Create new qustion of random type based on level.
		NSInteger limit_question = MODEL_QUESTION;
		
		switch (quizGame.level) {
			case NOOB:
				limit_question = MODEL_QUESTION;
				break;
			case APRENTICE:
				limit_question = MOTOR_QUESTION;
				break;
			case BASIC:
				limit_question = YEAR_QUESTION;
				break;
			case INTERMEDIATE:
				limit_question = TYPE_QUESTION;
				break;
			case EXPERT:
				limit_question = VELOCITY_QUESTION;
				break;
			case MASTER:
				limit_question = VELOCITY_QUESTION;
				break;
			default:
				break;
		}
		
		NSInteger new_question_type = (arc4random() % (limit_question + 1));
		// NSLog(@"%d", new_question_type);
		
		[quizGame newQuestionOfType:new_question_type];
		
		// For question text.
		[_lblQuestion setText:quizGame.question];
		
		// For question image.
		if (new_question_type != MOTOR_QUESTION) {
			[_imageAircraft setImage:[UIImage imageNamed:quizGame.question_image]];
		} else {
			[_imageAircraft setImage:[UIImage imageNamed:@"AirQuiz.png"]];
		}
		
		// NSLog(@"%@", quizGame.question_image);
		
		// Set game pickerview array.
		pickerTexts = [[NSMutableArray alloc] init];
		Aircraft *aircraft = quizGame.aircraftAnswer;
		NSArray *aircrafts;
		
		switch (new_question_type) {
			case MODEL_QUESTION:
				numberOfItensInPicker = 5;
				aircrafts = [quizGame getFourDifferentAircrafts];
				
				[pickerTexts addObject:quizGame.aircraftAnswer.model];
				[pickerTexts addObject:((Aircraft *) [aircrafts objectAtIndex:0]).model];
				[pickerTexts addObject:((Aircraft *) [aircrafts objectAtIndex:1]).model];
				[pickerTexts addObject:((Aircraft *) [aircrafts objectAtIndex:2]).model];
				[pickerTexts addObject:((Aircraft *) [aircrafts objectAtIndex:3]).model];
				
				// From http://stackoverflow.com/questions/56648/whats-the-best-way-to-shuffle-an-nsmutablearray
				for (NSUInteger i = 0; i < pickerTexts.count; ++i) {
					NSInteger remainingCount = pickerTexts.count - i;
					NSInteger exchangeIndex = i + arc4random_uniform((u_int32_t )remainingCount);
					[pickerTexts exchangeObjectAtIndex:i withObjectAtIndex:exchangeIndex];
				}
				
				for (NSInteger i = 0; i < numberOfItensInPicker; i++)
					if ([((NSString *) [pickerTexts objectAtIndex:i]) isEqualToString:quizGame.aircraftAnswer.model]) {
						correctAnswer = i;
						break;
					}
				
				break;
				
			case MOTOR_QUESTION:
				numberOfItensInPicker = 3;
				correctAnswer = aircraft.motor;
				[pickerTexts addObject:@"Monomotor"];
				[pickerTexts addObject:@"Bimotor"];
				[pickerTexts addObject:@"Turbo Helice"];
				
				break;
				
			case YEAR_QUESTION:
				numberOfItensInPicker = 5;
				
				[pickerTexts addObject:[NSString stringWithFormat:@"%d", aircraft.decade]];
				[pickerTexts addObject:[NSString stringWithFormat:@"%d", aircraft.decade + 10]];
				[pickerTexts addObject:[NSString stringWithFormat:@"%d", aircraft.decade + 20]];
				[pickerTexts addObject:[NSString stringWithFormat:@"%d", aircraft.decade - 10]];
				[pickerTexts addObject:[NSString stringWithFormat:@"%d", aircraft.decade - 20]];
				
				// From http://stackoverflow.com/questions/56648/whats-the-best-way-to-shuffle-an-nsmutablearray
				for (NSUInteger i = 0; i < pickerTexts.count; ++i) {
					NSInteger remainingCount = pickerTexts.count - i;
					NSInteger exchangeIndex = i + arc4random_uniform((u_int32_t )remainingCount);
					[pickerTexts exchangeObjectAtIndex:i withObjectAtIndex:exchangeIndex];
				}
				
				for (NSInteger i = 0; i < numberOfItensInPicker; i++)
					if ([((NSString *) [pickerTexts objectAtIndex:i]) isEqualToString:[NSString stringWithFormat:@"%d", aircraft.decade]]) {
						correctAnswer = i;
						break;
					}
				
				break;
				
			case TYPE_QUESTION:
				numberOfItensInPicker = 7;
				correctAnswer = aircraft.type;
				[pickerTexts addObject:@"Jato executivo"];
				[pickerTexts addObject:@"Turbo executivo"];
				[pickerTexts addObject:@"Multi uso"];
				[pickerTexts addObject:@"Agricola"];
				[pickerTexts addObject:@"Transporte de pessoas"];
				[pickerTexts addObject:@"Militar"];
				[pickerTexts addObject:@"Pessoal e/ou treinamento"];
				
				break;
				
			case VELOCITY_QUESTION:
				numberOfItensInPicker = 5;
				
				[pickerTexts addObject:[NSString stringWithFormat:@"%d", aircraft.velocity]];
				[pickerTexts addObject:[NSString stringWithFormat:@"%d", aircraft.velocity + 100]];
				[pickerTexts addObject:[NSString stringWithFormat:@"%d", aircraft.velocity + 200]];
				[pickerTexts addObject:[NSString stringWithFormat:@"%d", aircraft.velocity - 100]];
				[pickerTexts addObject:[NSString stringWithFormat:@"%d", aircraft.velocity - 200]];
				
				// From http://stackoverflow.com/questions/56648/whats-the-best-way-to-shuffle-an-nsmutablearray
				for (NSUInteger i = 0; i < pickerTexts.count; ++i) {
					NSInteger remainingCount = pickerTexts.count - i;
					NSInteger exchangeIndex = i + arc4random_uniform((u_int32_t )remainingCount);
					[pickerTexts exchangeObjectAtIndex:i withObjectAtIndex:exchangeIndex];
				}
				
				for (NSInteger i = 0; i < numberOfItensInPicker; i++)
					if ([((NSString *) [pickerTexts objectAtIndex:i]) isEqualToString:[NSString stringWithFormat:@"%d", aircraft.velocity]]) {
						correctAnswer = i;
						break;
					}
				
				break;
				
			default:
				NSLog(@"Não existe esse tipo de questão.");
				
				numberOfItensInPicker = 0;
				
				break;
		}
		
		[_picker reloadAllComponents];
	}
}

- (IBAction)sendResponse:(id)sender {
	if (selectedRow == correctAnswer) {
		// Increment score every correct answer.
		[userScore incrementScore];
		
		if (winCount < 6) {
			winCount++;
			
			happyPlay = YES;
			
			if (winCount == 6 && [userScore getLevel:quizGame.enterprize] <= quizGame.level) {
				[userScore incrementLevel:quizGame.enterprize];
				
				UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Parabéns" message:@"Você passou de nível! Deseja sair desse jogo?" delegate:self cancelButtonTitle:@"NÃO" otherButtonTitles:@"SIM", nil];
				[alert show];
			}
		}
	} else {
		if (winCount > 0 && winCount < 6) {
			winCount--;
			
			happyPlay = NO;
		}
	}
	
	[self updateView];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
	// 0 for NO.
	// 1 for YES;
	
	// Congratulations alert.
	if (buttonIndex == 1)
		[[self navigationController] popViewControllerAnimated:YES];
}

- (NSAttributedString *)pickerView:(UIPickerView *)pickerView attributedTitleForRow:(NSInteger)row forComponent:(NSInteger)component {
	return [[NSAttributedString alloc] initWithString:((NSString *) [pickerTexts objectAtIndex:row]) attributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
	selectedRow = row;
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
	return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
	return numberOfItensInPicker;
}

- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

@end
