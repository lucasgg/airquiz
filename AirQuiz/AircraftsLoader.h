//
//  AircraftsLoader.h
//  AirQuiz
//
//  Created by Lucas Guimaraes Gonsalves on 20/03/15.
//  Copyright (c) 2015 LGG. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, ENTERPRIZE) {
	CESSNA,
	EMBRAER
};

@class Aircraft;

@interface AircraftsLoader : NSObject

+ (id)sharedSingleton;

- (NSInteger)aircraftCount:(NSInteger)enterprize;
- (NSInteger)enterprizeCount;
- (Aircraft *)getAircraftFromEnterprize:(NSInteger)enterprize ofPosition:(NSInteger)pos;

@end
