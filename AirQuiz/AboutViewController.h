//
//  AboutViewController.h
//  AirQuiz
//
//  Created by Lucas Guimaraes Gonsalves on 18/03/15.
//  Copyright (c) 2015 LGG. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AboutViewController : UIViewController <UIAlertViewDelegate>

@end
