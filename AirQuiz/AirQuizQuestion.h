//
//  AirQuizQuestion.h
//  AirQuiz
//
//  Created by Lucas Guimaraes Gonsalves on 20/03/15.
//  Copyright (c) 2015 LGG. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, QUESTION_TYPE) {
	MODEL_QUESTION,
	MOTOR_QUESTION,
	YEAR_QUESTION,
	TYPE_QUESTION,
	VELOCITY_QUESTION
};

@interface AirQuizQuestion : NSObject

@property (readonly) NSString *question_text;
@property (readonly) BOOL isTextOnly;
@property (readonly) NSInteger type;

+ (AirQuizQuestion *)quizWithText:(NSString *)text andIsTextOnly:(BOOL)isTextOnly andType:(NSInteger)type;

- (instancetype)initWithText:(NSString *)text andIsTextOnly:(BOOL)isTextOnly andType:(NSInteger)type;

@end
