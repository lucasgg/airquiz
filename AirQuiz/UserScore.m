//
//  UserScore.m
//  AirQuiz
//
//  Created by Lucas Guimaraes Gonsalves on 20/03/15.
//  Copyright (c) 2015 LGG. All rights reserved.
//

#import "UserScore.h"

static UserScore *sharedSingleton = nil;

@implementation UserScore

+ (UserScore *)sharedSingletonFromScore:(NSInteger)score andLevels:(NSArray *)levels {
	// Create the shared singleton.
	static dispatch_once_t once;
	
	dispatch_once(&once, ^{
		sharedSingleton = [[UserScore alloc] initWithScore:score andLevels:levels];
	});
	
	return sharedSingleton;
}

+ (UserScore *)sharedSingleton {
	// Caution with this!
	// Initializate before with sharedSingletonFromScore.
	
	return sharedSingleton;
}

+ (NSArray *)defaultArrayOfLevels {
	// Initialize array of levels with NOOBs.
	NSMutableArray *playerLevels = [[NSMutableArray alloc] init];
	
	for (NSUInteger i = LEVEL_CESSNA; i <= LEVEL_ALL_AIRCRAFTS; i++) {
		// Add objects in array.
		[playerLevels addObject:[NSNumber numberWithInteger:NOOB]];
	}
	
	return (NSArray *) playerLevels;
}

- (instancetype)initWithScore:(NSUInteger)score andLevels:(NSArray *)levels {
	self = [super init];
	if (self) {
		_score = score;
		
		if (levels.count == LEVEL_ALL_AIRCRAFTS + 1)
			_levels = [NSMutableArray arrayWithArray:levels];
		else {
			NSLog(@"Problem in UserScore initialization: Array count is %d", levels.count);
			return nil;
		}
	}
	return self;
}

- (void)resetValuesToDefault {
	_score = 0;
	_levels = [NSMutableArray arrayWithArray:[UserScore defaultArrayOfLevels]];
}

- (void)incrementScore {
	// Increment the score by 1.
	_score++;
}

- (BOOL)indexIsValid:(NSInteger)aircraft {
	if (aircraft >= LEVEL_CESSNA && aircraft <= LEVEL_ALL_AIRCRAFTS)
		return YES;
	else
		return NO;
}

- (void)incrementLevel:(Levels)aircraft {
	// Verify and set the level.
	// This method made a verification on actual level to set the new level.
	
	if ([self indexIsValid:aircraft]) {
		NSInteger playerLevel = [[_levels objectAtIndex:aircraft] integerValue];
		
		if (playerLevel >= NOOB && playerLevel <= MASTER) {
			if (playerLevel < MASTER) {
				[_levels replaceObjectAtIndex:aircraft withObject:[NSNumber numberWithInteger:(playerLevel + 1)]];
			}
		} else {
			NSLog(@"The player level %d dont exist.", playerLevel);
		}
	} else {
		NSLog(@"The index %d dont exist.", aircraft);
	}
}

- (NSInteger)getLevel:(Levels)aircraft {
	// Return the player level of the aircraft level.
	// Return -1 if player level isn`t valid.
	
	if ([self indexIsValid:aircraft])
		return [[_levels objectAtIndex:aircraft] integerValue];
	else
		return -1;
}

- (NSString *)descriptionForLevel:(NSInteger)level {
	// Return text for level.
	switch (level) {
		case NOOB:
			return @"Noob";
		case APRENTICE:
			return @"Aprendiz";
		case BASIC:
			return @"Básico";
		case INTERMEDIATE:
			return @"Intermediário";
		case EXPERT:
			return @"Experiente";
		case MASTER:
			return @"Mestre";
		default:
			NSLog(@"Acesso a level não existente.");
			return @"Erro, level não existente.";
	}
}

- (NSString *)imageForAircraft:(NSInteger)aircraft andLevel:(NSInteger)level {
	return nil;
}

@end
