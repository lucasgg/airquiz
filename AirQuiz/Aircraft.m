//
//  Aircraft.m
//  AirQuiz
//
//  Created by Lucas Guimaraes Gonsalves on 20/03/15.
//  Copyright (c) 2015 LGG. All rights reserved.
//

#import "Aircraft.h"

@implementation Aircraft

+ (Aircraft *)aircraftWithModel:(NSString *)model andType:(NSInteger)type andMotor:(NSInteger)motor andDecade:(NSInteger)decade andWidth:(NSString *)width andVelocity:(NSInteger)velocity andImage:(NSString *)image {
	Aircraft *aircraft = [[Aircraft alloc] initWithModel:model andType:type andMotor:motor andDecade:decade andWidth:width andVelocity:velocity andImage:image];
	
	return aircraft;
}

- (instancetype)initWithModel:(NSString *)model andType:(NSInteger)type andMotor:(NSInteger)motor andDecade:(NSInteger)decade andWidth:(NSString *)width andVelocity:(NSInteger)velocity andImage:(NSString *)image {
	self = [super init];
	if (self) {
		_model = model;
		_type = type;
		_motor = motor;
		_decade = decade;
		_width = width;
		_velocity = velocity;
		_image = image;
	}
	return self;
}

@end
