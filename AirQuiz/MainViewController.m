//
//  MainViewController.m
//  AirQuiz
//
//  Created by Lucas Guimaraes Gonsalves on 19/03/15.
//  Copyright (c) 2015 LGG. All rights reserved.
//

#import "MainViewController.h"
#import "UserScore.h"
#import "AirQuizGame.h"

@interface MainViewController ()

@property (weak, nonatomic) IBOutlet UILabel *lblPontuacao;
@property (weak, nonatomic) IBOutlet UIView *viewCountainingScrollView;

@property (strong, nonatomic) IBOutletCollection(UILabel) NSArray *aircraftsLevelTexts;
@property (strong, nonatomic) IBOutletCollection(UIImageView) NSArray *aircraftsIconImages;

@end

@implementation MainViewController {
	UserScore *userScore;
	AirQuizGame *quizGame;
}

- (void)viewDidLoad {
	[super viewDidLoad];
	// Do any additional setup after loading the view.
	
	userScore = [UserScore sharedSingleton];
	quizGame = [AirQuizGame sharedSingleton];
	
	// Decorations...
	// Set an upper border to view countaining the scroll view.
	CALayer *upperBorder = [CALayer layer];
	upperBorder.backgroundColor = [[UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.25] CGColor];
	upperBorder.frame = CGRectMake(0, 0, CGRectGetWidth([_viewCountainingScrollView frame]), 0.5f);
	[[_viewCountainingScrollView layer] addSublayer:upperBorder];
}

- (void)viewWillAppear:(BOOL)animated {
	[self updateView];
}

- (void)updateView {
	// This is a custom function to update all labels, buttons and etc...
	
	// Set the score in label.
	_lblPontuacao.text = [NSString stringWithFormat:@"%d", userScore.score];
	
	// Set the user level.
	for (NSInteger i = LEVEL_CESSNA; i <= LEVEL_ALL_AIRCRAFTS; i++)
		((UILabel *) [_aircraftsLevelTexts objectAtIndex:i]).text = [userScore descriptionForLevel:[userScore getLevel:i]];
	
	// Set the level images.
	for (NSInteger i = LEVEL_CESSNA; i <= LEVEL_ALL_AIRCRAFTS; i++) {
		NSString *aircraft_icon_file_name;
		
		// Get image initial name.
		switch (i) {
			case LEVEL_CESSNA:
				aircraft_icon_file_name = @"cessna";
				break;
			case LEVEL_BOMBARDIER:
				aircraft_icon_file_name = @"bombardier";
				break;
			case LEVEL_AIRBUS:
				aircraft_icon_file_name = @"airbus";
				break;
			case LEVEL_BOEING:
				aircraft_icon_file_name = @"boeing";
				break;
			case LEVEL_EMBRAER:
				aircraft_icon_file_name = @"embraer";
				break;
			case LEVEL_ALL_AIRCRAFTS:
				aircraft_icon_file_name = @"allplanes";
				break;
			default:
				NSLog(@"Avião não existe para criar aircraft_icon_file_name.");
				break;
		}
		
		// Get user level.
		NSInteger level = [userScore getLevel:i];
		
		for (NSInteger j = NOOB; j < MASTER; j++) {
			NSString *number_of_aircrafts;
			
			// Get image second name.
			switch (j) {
				case NOOB:
					number_of_aircrafts = @"one";
					break;
				case APRENTICE:
					number_of_aircrafts = @"two";
					break;
				case BASIC:
					number_of_aircrafts = @"tree";
					break;
				case INTERMEDIATE:
					number_of_aircrafts = @"four";
					break;
				case EXPERT:
					number_of_aircrafts = @"five";
					break;
				default:
					NSLog(@"Nível está errado.");
					break;
			}
			
			// Verify if user already win a level.
			// If yes, get the diamond image, if not then get the white image.
			if (level > j) {
				[((UIImageView *) [_aircraftsIconImages objectAtIndex:((i * MASTER) + j)]) setHidden:NO];
				
				NSString *image = [NSString stringWithFormat:@"%@_%@_star_diamond.png", aircraft_icon_file_name, number_of_aircrafts];
				
				[((UIImageView *) [_aircraftsIconImages objectAtIndex:((i * MASTER) + j)]) setImage:[UIImage imageNamed:image]];
			} else if (level == j) {
				[((UIImageView *) [_aircraftsIconImages objectAtIndex:((i * MASTER) + j)]) setHidden:NO];
				
				NSString *image = [NSString stringWithFormat:@"%@_%@_star_white.png", aircraft_icon_file_name, number_of_aircrafts];
				
				[((UIImageView *) [_aircraftsIconImages objectAtIndex:((i * MASTER) + j)]) setImage:[UIImage imageNamed:image]];
			} else {
				[((UIImageView *) [_aircraftsIconImages objectAtIndex:((i * MASTER) + j)]) setHidden:YES];
			}
			
		}
	}
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
	// Go to game screen.
	
	// [touch view] = image button in question.
	for (NSInteger i = 0; i < _aircraftsIconImages.count; i++) {
		if ([touch view] == [_aircraftsIconImages objectAtIndex:i]) {
			//NSLog(@"%@", [touch view]);
			
			// Divisão de inteiro proposital.
			NSInteger aircraft = i / MASTER;
			NSInteger level = i % MASTER;
			
			//NSLog(@"%d - %d - %d", i, aircraft, level);
			
			[self toGameScreen:aircraft level:level];
		}
	}
	
	return YES;
}

- (void)toGameScreen:(NSInteger)aircraft level:(NSInteger)level {
	[quizGame createGameOfEnterprize:aircraft andLevel:level];
	
	// Perform segue to game screen.
	[self performSegueWithIdentifier:@"toGameScreen" sender:self];
}

- (IBAction)toAboutScreenButtonAction:(id)sender {
	// Call function to go to about screen.
	[self toAboutScreen];
}

- (void)toAboutScreen {
	// Go to about screen.
	[self performSegueWithIdentifier:@"toAboutScreen" sender:self];
}

- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

@end
