//
//  AirQuizGame.h
//  AirQuiz
//
//  Created by Lucas Guimaraes Gonsalves on 21/03/15.
//  Copyright (c) 2015 LGG. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Aircraft;

@interface AirQuizGame : NSObject

@property (readonly) NSInteger enterprize;
@property (readonly) NSInteger level;

@property (readonly) NSString *question;
@property (readonly) NSString *question_image;
@property (readonly) Aircraft *aircraftAnswer;

+ (id)sharedSingleton;

- (void)createGameOfEnterprize:(NSInteger)enterprize andLevel:(NSInteger)level;
- (void)newQuestionOfType:(NSInteger)question_type;
- (NSArray *)getFourDifferentAircrafts;

@end
